from django.shortcuts import render,get_object_or_404,redirect
from todos.models import TodoList,TodoItem
from todos.forms import TodoListForm,TodoItemForm

# Create your views here.

def todo_list_list (request):
    todolist= TodoList.objects.all()
    context={
        "list_object": todolist,
    }
    return render(request,"todos/list.html",context)

def todo_list_detail(request,id):
    details = get_object_or_404(TodoList, id = id)
    context = {
        "todo_object": details,
    }
    
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
  if request.method == "POST":
    form = TodoListForm(request.POST)
    if form.is_valid():
        todolist = form.save()
        return redirect("todo_list_detail",id=todolist.id)
  else:
    form = TodoListForm()

  context = {"form":form}

  return render(request, "todos/create.html", context)

def todo_list_edit(request,id):
  list = get_object_or_404(TodoList,id=id)
  if request.method == "POST":
    form = TodoListForm(request.POST,instance=list)
    if form.is_valid():
        form.save()
        return redirect("todo_list_detail",id=list.id)
  else:
    form = TodoListForm(instance=list)

  context = {
    "form":form,
    "todo_object":list,
   }

  return render(request,"todos/edit.html", context)


def todo_list_delete(request, id):
  model_instance = TodoList.objects.get(id=id)
  if request.method == "POST":
    model_instance.delete()
    return redirect ("todo_list_list")
  return render(request, "todos/delete.html")

def todo_item_create(request):
  if request.method == "POST":
    form = TodoItemForm(request.POST)
    if form.is_valid():
        item = form.save()
        return redirect("todo_list_detail",id=item.list.id)
  else:
    form = TodoItemForm()

  context = {"form":form}

  return render(request, "todos/create_list.html", context)

def todo_item_edit(request,id):
  item = get_object_or_404(TodoItem,id=id)
  if request.method == "POST":
    form = TodoItemForm(request.POST,instance=item)
    if form.is_valid():
        form.save()
        return redirect("todo_list_detail",id=item.list.id)
  else:
    form = TodoItemForm(instance=item)

  context = {
    "form":form,
    "todo_object":item,
   }

  return render(request,"todos/edit_item.html", context)
